package io.therms.rpc_client_kotlin_example

import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.checkbox.MaterialCheckBox
import io.therms.rpc_client_kotlin.client.RpcClient
import io.therms.rpc_client_kotlin.interfaces.EventLogger
import io.therms.rpc_client_kotlin.models.BaseResponse
import io.therms.rpc_client_kotlin.models.LoginResponse
import io.therms.rpc_client_kotlin.models.RpcException
import io.therms.rpc_client_kotlin.models.RpcResponse
import io.therms.rpc_client_kotlin.requests.BodyInterceptor
import io.therms.rpc_client_kotlin.requests.ErrorInterceptor
import io.therms.rpc_client_kotlin.requests.RpcRequest
import io.therms.rpc_client_kotlin.util.NetworkUtil
import io.therms.rpc_client_kotlin.util.Parameters
import org.json.JSONObject

class ExampleActivity : AppCompatActivity(), EventLogger {

    val TAG = "Websockets Sample"
    val WEB_SOCKET_URL = "wss://rpc.sandbox.therms.io/websocket"
    val HTTP_URL = "https://rpc.sandbox.therms.io"

    lateinit var rpcClient: RpcClient


    private val sendRequest by lazy {
        findViewById<Button>(R.id.send)
    }

    private val sendPayload by lazy {
        findViewById<Button>(R.id.sendPayload)
    }

    private val clearConsole by lazy {
        findViewById<Button>(R.id.clearConsole)
    }

    private val payload by lazy {
        findViewById<EditText>(R.id.payload)
    }

    private val cacheLookup by lazy {
        findViewById<MaterialCheckBox>(R.id.cacheLookup)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_example)

        rpcClient = RpcClient(applicationContext)
            .setHttpUrl(HTTP_URL)
            .setWebSocketUrl(WEB_SOCKET_URL)
            .shouldCacheResponses(true)
            .cacheMaxAge(15000)
            .useDuplicateInFlightCall(false)
            .setBodyInterceptor(object : BodyInterceptor {
                override fun intercept(body: JSONObject) {
                    val metadata = JSONObject().apply {
                        put(Parameters.APP_VERSION,getAppVersion())
                    }

                    val identity = JSONObject().apply {
                        put(Parameters.DEVICE_NAME, getDeviceName())
                        put(Parameters.METADATA, metadata)
                        //Adding authorization only when user is login
                        if(isUserLogin())
                            put(Parameters.AUTHORIZATION, getAuthorization())
                    }

                    if(!body.has(Parameters.IDENTITY))
                        body.put(Parameters.IDENTITY, identity)
                }
            })
            .setErrorInterceptor(object : ErrorInterceptor {
                override fun intercept(response: BaseResponse) {
                    //Authorization Errors can be intercepted from this central place
                    Log.d(TAG,"Error Code:" + response.code + " Error Message:" + response.message)
                    if(response.code == 401 || response.code == 403)
                        Toast.makeText(this@ExampleActivity,response.message,Toast.LENGTH_SHORT).show()
                }
            })
            .setEventListener(this)
            .build()

        sendRequest.setOnClickListener {
            sendLoginRequest()
        }

        sendPayload.setOnClickListener {
            sendPayloadRequest(payload.text.toString())
        }

        clearConsole.setOnClickListener {
            findViewById<TextView>(R.id.body).text = ""
        }
    }


    private fun sendLoginRequest() {
        val args = HashMap<String, Any>()
        args.let {
            it["email"] = "cory@therms.io"
            it["password"] = "123456"
        }
        val schema = NetworkUtil.createCallDTO("login", "auth", "1",  args)
        val request = RpcRequest<LoginResponse>(schema, LoginResponse::class.java)
            .listener(object : RpcResponse.Listener<LoginResponse?> {
                override fun onResponse(response: LoginResponse?) {
                    toConsole("onResponse() = " + response.toString())
                }

                override fun onError(error: RpcException?) {
                    toConsole("onError() = " + error?.message)
                }
            })
        if (cacheLookup.isChecked)
            rpcClient.callCache(request)
        else
            rpcClient.call(request)
    }

    private fun sendPayloadRequest(payload: String) {
        try {
            val schema = JSONObject(payload)
            val request = RpcRequest<LoginResponse>(schema, LoginResponse::class.java)
                .cacheLookup(true)
                .listener(object : RpcResponse.Listener<LoginResponse?> {
                    override fun onResponse(response: LoginResponse?) {
                        toConsole("onResponse() = " + response.toString())
                    }

                    override fun onError(error: RpcException?) {
                        toConsole("onError() = " + error?.message)
                    }
                })
            if (cacheLookup.isChecked)
                rpcClient.callCache(request)
            else
                rpcClient.call(request)
        } catch (ex: Exception) {
            Toast.makeText(this, "Enter Valid JSON", Toast.LENGTH_SHORT).show()
        }
    }

    private fun toConsole(text: String?) {
        findViewById<TextView>(R.id.body).let {
            it.text = it.text.toString() + "\n\n" + text

        }
    }

    override fun onLog(msg: String) {
        runOnUiThread {
            toConsole(msg)
        }
    }

    /**
     * Sandbox function
     */
    private fun isUserLogin(): Boolean{
        return true;
    }
    /**
     * Sandbox function for getting authorization
     */
    private fun getAuthorization(): String{
        return "testing"
    }

    private fun getAppVersion(): String{
        return "1.0"
    }

    /**
     * Method to get Device Name
     */

    private fun getDeviceName(): String {
        try {
            val deviceName = android.os.Build.MODEL
            val deviceMan = android.os.Build.MANUFACTURER
            return "$deviceMan $deviceName".replace(" ", "%20")
        } catch (ignore: Exception) {
        }
        return ""
    }
}